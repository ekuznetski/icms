/**
 * Created by Egor Kuznetski on 16/07/2018.
 */

WidgetFX = function (url, withStyles) {
    // check and update prices only after page loaded, for be sure that .widget-container already in DOM
    document.addEventListener('DOMContentLoaded', function () {
        // fire function first time for render widget
        updateWidget();
        // fire the same function for update widget every second
        setInterval(function () {
            updateWidget();
        }, 1000);
    });
    window.addEventListener('resize', function () {
        // fire function with attribute for render widget with new cells position
        updateWidget('needRender');
    });

    // from https://stackoverflow.com/questions/123999/how-to-tell-if-a-dom-element-is-visible-in-the-current-viewport/7557433#7557433
    function isElementInViewport(el) { // check is element in viewport, if part of element in viewport, this is work like full element in viewport
        var top = el.offsetTop;
        var left = el.offsetLeft;
        var width = el.offsetWidth;
        var height = el.offsetHeight;

        while(el.offsetParent) {
            el = el.offsetParent;
            top += el.offsetTop;
            left += el.offsetLeft;
        }

        return (
            top < (window.pageYOffset + window.innerHeight) &&
            left < (window.pageXOffset + window.innerWidth) &&
            (top + height) > window.pageYOffset &&
            (left + width) > window.pageXOffset
        );
    }

    // convert timestamp from csv to time and add leading zeros
    function getPriceTime(time) {
        time = new Date(parseInt(time));

        function addZero(value) {
            if (value < 10) {
                return '0' + value;
            } else {
                return value;
            }
        }
        return addZero(time.getHours()).toString() + ':' + addZero(time.getMinutes()).toString() + ':' + addZero(time.getSeconds()).toString();
    }


    // prepare symbol name for using like class name
    function splitSymbol(symbol) {
        return symbol.split('/')[0].toLowerCase() + '-' + symbol.split('/')[1].toLowerCase();
    }

    // render row in table
    function renderRow(pair, bid, ask, time) {
        var wH = window.innerWidth,
            row = '',
            spread = (bid - ask).toFixed(6);
        time = getPriceTime(time);

        // in requirements was wrote that we should ensure work in IE9
        // if we not need support ie9, ie10 we would be use flexbox with order parameters for change row positions for different screens
        // but for support ie9 we should use only JS because we have different positions for table cells on different devices and we need render tables for different sizes

        if (wH >= 600) {
            row = '<tr class="' + splitSymbol(pair) + '">\n' +
                '      <td class="widget_col wdgt_pair">' + pair + '</td>\n' +
                '      <td class="widget_col wdgt_bid">' + bid + '</td>\n' +
                '      <td class="widget_col wdgt_ask">' + ask + '</td>\n' +
                '      <td class="widget_col wdgt_spread">' + spread + '</td>\n' +
                '      <td class="widget_col wdgt_time">' + time + '</td>\n' +
                '  </tr>';
        } else if (wH >= 400 && wH < 600) {
            row = '<tr class="' + splitSymbol(pair) + '">\n' +
                '      <td rowspan="2" class="widget_col wdgt_pair">' + pair + '</td>\n' +
                '      <td rowspan="1" class="widget_col wdgt_bid">' + bid + '</td>\n' +
                '      <td rowspan="1" class="widget_col wdgt_spread">' + spread + '</td>\n' +
                '  </tr>\n' +
                '  <tr class="' + splitSymbol(pair) + '">\n' +
                '      <td rowspan="1" class="widget_col wdgt_ask">' + ask + '</td>\n' +
                '      <td rowspan="1" class="widget_col wdgt_time">' + time + '</td>\n' +
                '  </tr>\n';
        } else {
            row = '<tr class="' + splitSymbol(pair) + '"><td class="widget_col wdgt_pair">' + pair + '</td></tr>\n' +
                '  <tr class="' + splitSymbol(pair) + '"><td class="widget_col wdgt_time">' + time + '</td></tr>' +
                '  <tr class="' + splitSymbol(pair) + '"><td class="widget_col wdgt_spread">' + spread + '</td></tr>\n' +
                '  <tr class="' + splitSymbol(pair) + '"><td class="widget_col wdgt_ask">' + ask + '</td></tr>\n' +
                '  <tr class="' + splitSymbol(pair) + '"><td class="widget_col wdgt_bid">' + bid + '</td></tr>\n';
        }
        return row
    }

    function updateRow(pair, bid, ask, time) {
        time = getPriceTime(time);
        var spread = (bid - ask).toFixed(6),
            // prepare object with data from csv, for compare with existing data
            elements = {'time': time, 'spread': spread, 'ask': ask, 'bid': bid};

        for (var key in elements) {
            var val = elements[key],
                element = document.querySelector('.' + splitSymbol(pair) + ' .wdgt_' + key);
            //compare each existing forex symbol value with data from csv, if it not equal, will update it
            if (element.innerHTML !== val) {
                // remove previous class, add new class by condition and update value
                element.className = element.className.replace(' up', '').replace(' down', '');
                if (element.innerHTML > val) {
                    element.className += ' down'
                } else if (element.innerHTML < val) {
                    element.className += ' up'
                }
                element.innerHTML = val;
            }
        }

    }

    // I putted updateRow() and renderRow() functions inside updateWidget()
    // for prevent creating global variable for save prices after request and
    // sending this variable like attribute to this two functions.
    // Attribute render mean that need render widget after resizing or page loading
    function updateWidget(needRender) {
        // create new object for make ajax request to php, which response csv converted to json,
        // because cross origin requests not allowed in headers on truefx page
        var xhr = new XMLHttpRequest(),
            container = document.getElementsByClassName('widget-container')[0],
            prices,
            widgetExist = document.getElementById('widget_wrap');
        // url - this variable contain url to php file. can be changed to any url with json with the same structure
        // opening asynchronous connection to url
        // added random get parameter 'h' for prevent caching in IE
            xhr.open('GET', url+'?h='+Math.random(), true);
            // fire function after loading requested page
            xhr.onload = function () {
                // if status success, start prepare data and fire functions
                if (xhr.status === 200) {
                    // sort by symbol name
                    prices = JSON.parse(xhr.responseText).sort();
                    var rows = '';
                    for (var i in prices) {
                        // split each string to array
                        var rowArr = prices[i].split(',');
                        if (rowArr[7] !== undefined && rowArr[6] !== undefined) {
                            // check render widget or only update
                            if (widgetExist) {
                                if (needRender) {
                                    rows += renderRow(rowArr[0], rowArr[7], rowArr[6], rowArr[1]);
                                } else {
                                    // update only if any part or full element is visible (in viewport)
                                    if (isElementInViewport(container)) {
                                        updateRow(rowArr[0], rowArr[7], rowArr[6], rowArr[1])
                                    }
                                }
                            }
                            if (!widgetExist) {
                                rows += renderRow(rowArr[0], rowArr[7], rowArr[6], rowArr[1]);
                            }
                        }
                    }
                    if (!widgetExist || needRender) { // wrap rows if it's new table
                        container.innerHTML = '<table id="widget_wrap">' + rows + '</table>';
                    }
                }
                else {// console log if ajax request failed
                    console.log('Failed\n' + xhr.status);
                }
            };
            xhr.send(); // send request
    }
    // add styles for table, this styles can be disabled when we add new widget
    function addStyles(css) {
        var head = document.getElementsByTagName('head')[0],
            s = document.createElement('style');
        s.setAttribute('type', 'text/css');
        if (s.styleSheet) { // this is fix for IE
            s.styleSheet.cssText = css; // this is fix for IE
        } else {
            s.appendChild(document.createTextNode(css));
        }
        head.appendChild(s);
    }

    if (withStyles) {
        addStyles('\n' +
            '.widget_col.down:not(.wdgt_time){\n' +
            '   color: red;\n' +
            '}\n' +
            '.widget_col.up:not(.wdgt_time){\n' +
            '   color: green;\n' +
            '}\n' +
            'table#widget_wrap{\n' +
            '    border-collapse: collapse;\n' +
            '    border-spacing: 0;\n' +
            '}\n' +
            '.wdgt_pair{\n' +
            '   font-weight: bold\n' +
            '}\n' +
            '.widget_col{\n' +
            '   border:1px solid black;\n' +
            '   padding: 1px 5px;\n' +
            '}\n' +
            '.widget_row{\n' +
            '}\n');
    }

};

// first attr set url for parsing (php or webrates.truefx.com),
// second attr set use default style or not use
var widget = new WidgetFX('parse_csv.php', true);